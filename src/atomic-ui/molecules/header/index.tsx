import React, { useEffect, useState } from "react";
import { Link, NavLink } from "react-router-dom";
import { HeaderHtml, NavUnlisted } from "./style";

const Header = () => {
    const [search, setSearch] = useState("");
    let [scroll, setScroll] = useState(window.scrollY);
    let [postion, setPostion] = useState("0px");
    let [lastScroll, setLastScroll] = useState(0);
    
    
    
    useEffect(() => {
    const teste = () => {
        setScroll(window.scrollY);
            if(lastScroll > scroll) {
                setPostion("0");
            }else {
                setPostion("-50px");
            }
        setLastScroll(window.scrollY);
    }

    window.addEventListener("scroll", teste);  
    return () => {
        window.removeEventListener("scroll",teste); 
    };
},[scroll])


    return <>
        <div style={{height: 50, backgroundImage: "linear-gradient(to right, #032541 , #07bbd7, #19d0b3)"}}></div>
        <HeaderHtml position={postion}>
            <NavUnlisted>
                <NavLink to="/home" className={(navData) => navData.isActive ? "current" : "" } >
                    <li>HOME</li>
                </NavLink>
                <NavLink to="/groups" className={(navData) => navData.isActive ? "current" : "" }>
                    <li>GROUPS</li>
                </NavLink>
                <NavLink to="/" className={(navData) => navData.isActive ? "current" : "" }>
                    <li>ERROR</li>
                </NavLink>
            </NavUnlisted>
        </HeaderHtml>
    </>
}

export default Header;