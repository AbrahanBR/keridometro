import { Paper } from "@mui/material";
import React, { useEffect, useState } from "react";
import AvatarAtom from "../../atoms/avatar";
import { Props } from "./types";
import Queridometro from "../../atoms/queridometro";
import { Wrap } from "./style";

const VoteMolecule = (props: Props) => {
    const {name} = props;
    const [emojis, setEmoji] = useState("");

    useEffect(() => {
        setEmoji(emojis)
    },[emojis])
    return <>
        <Wrap>
            <Paper elevation={5} sx={[{margin: "2.5px"}]}>
                <div style={{display: "flex", alignItems: "center", padding: "0 10px"}}>
                    <AvatarAtom name={name}/>
                    <h3>{name}</h3>
                    <p>{emojis}</p>
                </div>
                <Queridometro handleChangeEmoji={emojis => setEmoji(emojis)}/>
            </Paper>
        </Wrap>
    </>
}

export default VoteMolecule