import React, { useState } from "react";
import { pink } from '@mui/material/colors';
import Radio from '@mui/material/Radio';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBacon, faPoo, faFaceSmile, faFaceAngry, faHeart, faHeartCrack, faBomb, faPepperHot, faSeedling } from "@fortawesome/free-solid-svg-icons";
import { Props } from "./types";


const Queridometro = (props:Props) => {
    const { handleChangeEmoji } = props;
    const [selectedValue, setSelectedValue] = useState("a");

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSelectedValue(event.target.value);
    };

    const controlProps = (item: string) => ({
        onchange: handleChangeEmoji(item),
        checked: selectedValue === item,
        onChange: handleChange,
        value: item,
        name: 'color-radio-button-demo',
        inputProps: { 'aria-label': item },
        console: console.log(item)
    });

    return (
    <div>
        <Radio {...controlProps('snake')} color="success" icon={<FontAwesomeIcon icon={faBacon} size="lg"/>} checkedIcon={<FontAwesomeIcon icon={faBacon} size="lg"/>}/>
        <Radio {...controlProps('pilk')} color="secondary" icon={<FontAwesomeIcon icon={faPoo} size="lg"/>} checkedIcon={<FontAwesomeIcon icon={faPoo} size="lg"/>}/>
        <Radio {...controlProps('smile')} color="warning" icon={<FontAwesomeIcon icon={faFaceSmile} size="lg"/>} checkedIcon={<FontAwesomeIcon icon={faFaceSmile} size="lg"/>}/>
        <Radio {...controlProps('angry')} color="error" icon={<FontAwesomeIcon icon={faFaceAngry} size="lg"/>} checkedIcon={<FontAwesomeIcon icon={faFaceAngry} size="lg"/>}/>
        <Radio {...controlProps('bomb')} color="primary" icon={<FontAwesomeIcon icon={faBomb} size="lg"/>} checkedIcon={<FontAwesomeIcon icon={faBomb} size="lg"/>}/>
        <Radio {...controlProps('heart')} color="error" icon={<FontAwesomeIcon icon={faHeart} size="lg"/>} checkedIcon={<FontAwesomeIcon icon={faHeart} size="lg"/>}/>
        <Radio {...controlProps('brokenheart')} color="secondary" icon={<FontAwesomeIcon icon={faHeartCrack} size="lg"/>} checkedIcon={<FontAwesomeIcon icon={faHeartCrack} size="lg"/>}/>
        <Radio {...controlProps('banana')} color="warning" icon={<FontAwesomeIcon icon={faPepperHot} size="lg"/>} checkedIcon={<FontAwesomeIcon icon={faPepperHot} size="lg"/>}/>
        <Radio {...controlProps('plant')} color="success" icon={<FontAwesomeIcon icon={faSeedling} size="lg"/>} checkedIcon={<FontAwesomeIcon icon={faSeedling} size="lg"/>}/>
    </div>
    );
}

export default Queridometro;
