import * as React from 'react';
import Accordion from '@mui/material/Accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import AccordionSummary from '@mui/material/AccordionSummary';
import Typography from '@mui/material/Typography';
import { faArrowDown } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import AvatarAtom from '../../atoms/avatar';
import { Props } from './types';
import { List, ListItem, ListItemAvatar, ListItemText } from '@mui/material';

const GroupsMol = (props: Props) => {
  const {groupName, members} = props;

  const [expanded, setExpanded] = React.useState<string | false>(false);

  const handleChange = (panel: string) => (event: React.SyntheticEvent, isExpanded: boolean) => {
      setExpanded(isExpanded ? panel : false);
  }

  return (
    <div>
      <Accordion expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>

          <AccordionSummary
            expandIcon={<FontAwesomeIcon icon={faArrowDown}/>}
            aria-controls="panel1bh-content"
            id="panel1bh-header"
          >
            <Typography sx={{ width: '15%', flexShrink: 0 }}>
              <div style={{display: "flex", alignItems: "center"}}>
                <AvatarAtom name={groupName} /> 
                {groupName}
              </div>
            </Typography>
            <Typography sx={{ color: 'text.secondary' }}>
              {
                //@ts-ignore
                members.map((member: string) => {
                  return `${member}, `})
              }
            </Typography>
          </AccordionSummary>
          
        <AccordionDetails>
        <Typography>

          <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
            {
              //@ts-ignore
              members.map((member: any) => {
                  return <ListItem>

                    <ListItemAvatar>
                      <AvatarAtom name={member} />
                    </ListItemAvatar>

                    <ListItemText primary={member} secondary="Jan 9, 2014" />
                  </ListItem>
              })
            }
          </List>

        </Typography>
        </AccordionDetails>

      </Accordion>
    </div>
  );
}

export default GroupsMol;