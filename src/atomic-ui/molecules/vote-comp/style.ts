import styled from "@emotion/styled";

export const Wrap = styled.div`
    display: flex;
    flex-direction: column;
    flex: 1 1 auto;
    align-items: stretch;
`