import React from 'react';
import { Routes, Route } from 'react-router-dom';
import Footer from './atomic-ui/molecules/footer';
import GroupsMol from './atomic-ui/molecules/groups';
import Header from './atomic-ui/molecules/header';
import GroupListOrg from './atomic-ui/organisms/group-list';

const  App = () => {
  const teste = ["lucas gomes", "anabele teste", "anabele teste", "anabele teste", "anabele teste", "anabele teste", "anabele teste"];

  return (
    <>
      <Header /> 

      <Routes>
        {/*@ts-ignore */}
        <Route path="/home" element={<GroupListOrg participants={teste}/>}/>
        {/*@ts-ignore */}
        <Route path="/groups" element={<GroupsMol groupName="Group Test" members={teste} />} />
        <Route
          path="/"
          element={
            <main style={{ marginTop: "1rem", display:"flex",justifyContent:'center' }}>
              <h1>Oh No. Something Goes Wrong</h1>
            </main>
          }
        />
      </Routes>

      <Footer />
    </>
  );
}

export default App;
