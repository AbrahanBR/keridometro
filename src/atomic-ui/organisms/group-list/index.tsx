import React from "react";
import VoteMolecule from "../../molecules/vote-comp";
import { Props } from "./types";

const GroupListOrg = (props: Props) => {
    const {participants} = props;
    return <div style={{padding: "10px", display: "flex", flexWrap: "wrap", maxWidth: "1200px"}}>{
            /*@ts-ignore */
            participants.map((participant: string) => <VoteMolecule name={participant}/>)
        }</div>
    
}

export default GroupListOrg;